//
//  MyCustomAnnotation.m
//  CustmPin
//
//  Created by Student-002 on 07/09/16.
//  Copyright © 2016 student 002. All rights reserved.
//

#import "MyCustomAnnotation.h"

@implementation MyCustomAnnotation

@synthesize coordinate;

-(id)initWithLocation:(CLLocationCoordinate2D)coord
{
    self = [super init];
    
    if (self) {
        
        coordinate = coord;
        
    }
    
    return self;
}


@end
