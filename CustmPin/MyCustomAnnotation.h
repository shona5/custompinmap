//
//  MyCustomAnnotation.h
//  CustmPin
//
//  Created by Student-002 on 07/09/16.
//  Copyright © 2016 student 002. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MyCustomAnnotation : NSObject<MKAnnotation>
{
    CLLocationCoordinate2D coordinate;
    
}
@property(nonatomic,readonly)CLLocationCoordinate2D coordinate;

-(id)initWithLocation:(CLLocationCoordinate2D)coord;
@end
